# Generating a new private key and CSR

## Define the key_and_csr function

Copy-paste the the following shell functinos into a root console session on the
web server. This will define a shell function called `key_and_csr`.

```


key_and_csr()
{
  # Start a subshell to avoid tainting the interactive shell with umask and
  # variables.
  (
  domain="${1}"
  conf_file="${domain}.conf"
  csr_file="${domain}.csr"

  # Make sure that the files we generate are not group- or world-readable
  umask 077

  # Generate foo.example.com.conf with the default CSR details
  _key_and_csr_write_conf "${domain}" "${conf_file}"

  # Create foo.example.com.key and foo.example.com.csr
  _key_and_csr_abort_if_failed openssl req -new -config "${conf_file}" -out "${csr_file}"

  # Print the CSR for inspection
  _key_and_csr_abort_if_failed openssl req -in "${csr_file}" -text
  )
}

_key_and_csr_write_conf()
{
  _key_and_csr_abort_if_failed tee "${2}" > /dev/null <<EOF
[req]
prompt = no
distinguished_name = dn
req_extensions = ext
default_bits = 2048
default_keyfile = ${1}.key
default_md = sha256
encrypt_key = no

[dn]
C = NL
ST = Utrecht
L = Utrecht
O = GitLab B.V.
CN = ${1}
emailAddress = sytse@gitlab.com

[ext]
subjectAltName = DNS:${1}
EOF
}

_key_and_csr_abort_if_failed()
{
  if ! "$@" ; then
    echo "ERROR: command failed: $*"
    exit 1
  fi
}


```

## Generate the key and certificate

In the same root console you used above, run `key_and_csr foo.example.com`.
This will create/overwrite `foo.example.com.key` and `foo.example.com.csr`.


## Use the CSR to request a certificate

After running `key_and_csr foo.example.com`, copy-paste the CSR
(`BEGIN`...`END`) and submit it to the CA.
